import os
import re
import yaml
import time
import shutil

def convert_to_gb(t):
    return {k.capitalize(): v // (2**30) for k,v in t}

def get_config():
    path, _ = os.path.split(os.path.realpath(__file__))
    with open(os.path.join(path, 'config.yaml')) as file:
        return yaml.full_load(file)

def get_dir_config():    
    return get_config()["dir"]

def get_size():
    return get_config()["size"]
    
def get_exts():
    return get_config()["exts"]

def get_interval():
    return get_config()["interval"]

def get_space():
    return convert_to_gb(shutil.disk_usage("/")._asdict().items())
get_free_space = lambda: get_space()["Free"]

def clean():
    if get_free_space() < get_size():
        for d in get_dir_config():
            for (root, _, files) in os.walk(d):
                for f in files:
                    for ext in get_exts():
                        if re.match(f".*\.{ext}$", f):
                            t_path = os.path.join(root, f)
                            print(t_path)
                            os.remove(t_path)

while True:
    clean()
    time.sleep(get_interval())
