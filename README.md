# disk_cleaner

Clean up configured folders if free space on the disk is less than expected

## Config example:

```yaml
interval: 10
size: 200
exts:
  - txt
dir:
  - C:\my\logs
  - C:\my\wf
```